import com.github.javafaker.Faker;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import scheduler.*;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        DobGenerator dobGenerator = new DobGenerator();
        RandomStringGenerator strGen = new RandomStringGenerator();
        Faker faker = new Faker();

        // Manually initialize 3 trainers
        List<Trainer> trainerList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            trainerList.add(new Trainer(faker.name().firstName(),
                    faker.name().lastName(),
                    dobGenerator.randomBetween(1980, 1990)));
        }

        // Manually initialize 15 students
        List<Student> studentList = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            studentList.add(new Student(faker.name().firstName(),
                    faker.name().lastName(), dobGenerator.randomBetween(1970, 2002), new Random().nextBoolean()));
        }

        // Manually initialize 4 groups
        List<Group> groupList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            groupList.add(new Group(strGen.generate("ABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890", 4)));
        }

        // Assign a trainer to each group
        groupList.forEach(group -> group.setTrainer(trainerList.get(new Random().nextInt(trainerList.size()))));

        // Assign 2-3 students to each group
        // Ensure the fact that a group will only have distinct students
        /* Ensure the fact that a group will only have a maximum of 5 students;
        When you try to add a 6th one throw an MaximumNumberOfStudentsReached exception */
        groupList.forEach(group -> {
            for (int i = 0; i < (2 + new Random().nextInt(5)); i++) {
                if (studentList.isEmpty()) {
                    throw new ListOfStudentsIsEmptyException("No more students in list for group creation! " +
                            "Generation of group is aborted!");
                } else {
                    if (i == 6) {
                        throw new MaximumNumberOfStudentsReachedException("Group has a maximum of 5 students. " +
                                "Generation of group is aborted!");
                    } else {
                        Student random = studentList.get(new Random().nextInt(studentList.size()));
                        studentList.remove(random);
                        group.addStudent(random);
                    }
                }
            }
        });

        System.out.println("Randomly generated groups: ");
        groupList.forEach(System.out::println);

        // Display all students in a group sorted alphabetically by lastName
        System.out.println("\nStudents in a group sorted alphabetically by lastName:");
        Map<Group, List<Student>> groupStudentsMap = new HashMap<>();
        groupList.forEach(group -> group.getStudentList().stream()
                .sorted()
                .forEach(student ->
                updateValue(groupStudentsMap, group, student)));

        groupStudentsMap.forEach((group, students) ->
                System.out.println("Group{name='" + group.getName() + "'} : " + students));

        // Display the group with the maximum number of students [***EXTRA*** sorted alphabetically by lastName]
        System.out.println("\nThe group with the maximum number of students sorted alphabetically by lastName:");
        Map<Group, List<Student>> groupStudentListMap = new HashMap<>();

        Group gr = groupList.stream()
                .sorted((gr1, gr2) -> Integer.compare(gr2.getStudentList().size(), gr1.getStudentList().size()))
                .findFirst()
                .get();

        groupStudentListMap.put(gr, gr.getStudentList().stream()
                .sorted(Person::compareTo).collect(Collectors.toList()));
        groupStudentListMap.forEach((group, students) ->
                System.out.println("Group{name='" + group.getName() + "'} : " + students));

        // ***EXTRA***
        System.out.print("Largest group's size is: ");
        groupList.stream()
                .map(Group::getStudentList)
                .mapToInt(List::size)
                .max()
                .ifPresent(System.out::println);

        /* Display all students grouped by trainer that teaches to them
         (eg. Trainer1 - stud1, stud3, stud4; Trainer2 - stud2, stud10) - regardless of the group they're part of*/
        System.out.println("\nStudents grouped by trainer that teaches to them:");
        Map<Trainer, List<Student>> trainerStudentsMap = new HashMap<>();

        groupList.forEach(group -> group.getStudentList().forEach(student ->
                updateValue(trainerStudentsMap, group.getTrainer(), student)));

        trainerStudentsMap.forEach((tr, st) ->
                System.out.println("Trainer: " + tr + " teaches: " + st));

        System.out.println("***EXTRA*** solution with multimap library:");
        Multimap<Trainer, List<Student>> multimap = ArrayListMultimap.create();
        groupList.forEach(group -> multimap.put(group.getTrainer(),
                group.getStudentList()));

        for (Trainer trainer : multimap.keySet()) {
            System.out.println("Trainer: " + trainer + " teaches: " + multimap.get(trainer));
        }
//        or
//        for (Map.Entry<Trainer, Collection<List<Student>>> e : multimap.asMap().entrySet()) {
//            System.out.println(e.getKey() + " : " + e.getValue());
//        }

        // Display all students younger than 25, from all groups [***EXTRA*** sorted alphabetically by lastName]
        System.out.println("\nAll students younger than 25, from all groups, sorted alphabetically by firstName:");
        groupList.stream()
                .map(Group::getStudentList)
                .flatMap(List::stream)
                .sorted(Comparator.comparing(Person::getFirstName))
                .filter(student -> Period.between(student.getDateOfBirth(), LocalDate.now()).getYears() < 25)
                .forEach(System.out::println);

        // Display all students with previous java knowledge
        System.out.println("\nStudents with previous java knowledge:");
        groupList.stream()
                .map(Group::getStudentList)
                .flatMap(List::stream)
                .filter(Student::isHasPreviousJavaKnowledge)
                .forEach(System.out::println);

        // Display the group with the highest number of students with no previous java knowledge
        System.out.println("\nGroup with the highest number of students with no previous java knowledge:");
        groupList.stream()
                .sorted((gr1, gr2) -> Long.compare(gr2.getStudentsWithoutPreviousJavaKnowledge(),
                        gr1.getStudentsWithoutPreviousJavaKnowledge()))
                .findFirst()
                .ifPresent(System.out::println);

        // ***EXTRA***
        // Display students with no previous java knowledge of the largest group
        System.out.println("\nStudents with no previous java knowledge of the largest group:");
        groupList.stream()
                .sorted((gr1, gr2) -> Integer.compare(gr2.getStudentList().size(), gr1.getStudentList().size()))
                .findFirst()
                .map(Group::getStudentList)
                .ifPresent(students -> students.stream()
                        .filter(student -> !student.isHasPreviousJavaKnowledge())
                        .forEach(System.out::println));

        // Remove all the students younger than 20 from all groups
        System.out.println("\nRemoving students younger than 20 from all groups... Done.");
        groupList.forEach(Group::removeByAge); // removing students younger than 20 from all groups
        System.out.println("Students left after removing:");
        groupList.stream()
                .map(Group::getStudentList)
                .flatMap(List::stream)
                .forEach(System.out::println);
    }

    private static <KEY, VALUE> void updateValue(Map<KEY, List<VALUE>> map, KEY key, VALUE value) {
        map.compute(key, (k, values) -> values == null ? new ArrayList<>() : values).add(value);
    }
}