package scheduler;

import java.util.Random;

public class RandomStringGenerator {
    final Random random = new Random();

    public String generate(String lexicon, int length) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            builder.append(lexicon.charAt(random.nextInt(lexicon.length())));
        }
        return builder.toString();
    }
}