package scheduler;

import java.time.LocalDate;
import java.util.Random;

public class DobGenerator {
    public LocalDate randomBetween(int start, int end) {
        int from = (int) LocalDate.of(start, 1, 1).toEpochDay();
        int to = (int) LocalDate.of(end, 1, 1).toEpochDay();
        if (start >= end || start < 0) {
            throw new PeriodMustBePositiveException("Wrong inputs!");
        } else {
            long period = from + new Random().nextInt(to - from);
            return LocalDate.ofEpochDay(period);
        }
    }
}