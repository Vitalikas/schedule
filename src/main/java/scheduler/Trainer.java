package scheduler;

import java.time.LocalDate;

public class Trainer extends Person implements isAuthorizable {
    public Trainer(String firstName, String lastName, LocalDate dateOfBirth) {
        super(firstName, lastName, dateOfBirth);
    }

    @Override
    public boolean isAuthorized() {
        return true;
    }
}
