package scheduler;

public class ListOfStudentsIsEmptyException extends IllegalArgumentException {
    public ListOfStudentsIsEmptyException(String s) {
        super(s);
    }
}