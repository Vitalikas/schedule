package scheduler;

public interface isAuthorizable {
    boolean isAuthorized();
}
