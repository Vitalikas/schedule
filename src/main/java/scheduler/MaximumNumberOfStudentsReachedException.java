package scheduler;

public class MaximumNumberOfStudentsReachedException extends IllegalArgumentException {
    public MaximumNumberOfStudentsReachedException(String s) {
        super(s);
    }
}
