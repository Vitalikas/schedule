package scheduler;

public class PeriodMustBePositiveException extends IllegalArgumentException {
    public PeriodMustBePositiveException(String message) {
        super(message);
    }
}