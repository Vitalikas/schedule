package scheduler;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Group {
    private final String name;
    private Trainer trainer;
    private final List<Student> studentList = new ArrayList<>();

    public Group(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public void addStudent(Student student) {
        studentList.add(student);
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", trainer=" + trainer +
                ", studentList=" + studentList +
                '}';
    }

    public long getStudentsWithoutPreviousJavaKnowledge() {
        return studentList
                .stream()
                .filter(student -> !student.isHasPreviousJavaKnowledge())
                .count();
    }

    public void removeByAge() {
        studentList
                .removeIf(student -> Period.between(student.getDateOfBirth(),
                        LocalDate.now()).getYears() < 20);
    }
}